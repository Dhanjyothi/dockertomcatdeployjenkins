package net.guides.springboot2.springboot2webappjsp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
//#https://www.javainuse.com/devOps/docker/docker-war
@Controller
public class UserController {
	

	@RequestMapping("/users")
	public String home(Model model) {
	
		return "users";
	}
}
